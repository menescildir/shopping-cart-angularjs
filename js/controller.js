var app = angular.module('shoppingcart.controller',[]);
			app.controller('addCtrl', function($scope){

		$scope.items = [];
		$scope.myitems = [];

		$scope.addItem = function(){
			$scope.items.push({
				number : $scope.product_number,
				name : $scope.product_name,
				price : $scope.product_price,
				currency : $scope.currency
			});
			$scope.product_number = "";
			$scope.product_name = "";
			$scope.product_price = "";
			$scope.currency = "";
		};


		$scope.addmyitems = function (item) {

			if($scope.myitems.length == 0) {
				item.count = 1;
				$scope.myitems.push(item);
			}else {
				 var repeating = false;
				for(var i=0; i < $scope.myitems.length; i++){
					if($scope.myitems[i].number == item.number){
						repeating = true;
						$scope.myitems[i].count++;

					}
				}
				if(!repeating){
					item.count = 1;
					$scope.myitems.push(item);
				}
				
				
			}
			$scope.totalPrice();
		};
			$scope.totalPrice = function () {
				var total = 0;
				var dollar_val = 2.93;
				var euro_val = 3.32;
				for(var i = 0; i < $scope.myitems.length; i++){
					if($scope.myitems[i].currency == "Dollar"){
					total += ($scope.myitems[i].count * $scope.myitems[i].price) * dollar_val;
					}else if($scope.myitems[i].currency == "Euro"){
					total += ($scope.myitems[i].count * $scope.myitems[i].price) * euro_val;
					}
				}
				$scope.total = total.toFixed(2);
			}


	});